/**************************************************************************//**
 * @file     log_api.h
 * @brief    日志、打印和调试信息
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _LOG_API_H_
#define _LOG_API_H_

typedef enum {
    LOG_LEVEL_DEBUG,
    LOG_LEVEL_INFO,
    LOG_LEVEL_WARNING,
    LOG_LEVEL_FATAL
} EN_LOG_LEVEL;

void LOG_Print(const char *pfuncName, EN_LOG_LEVEL level, const char *format, ...);

#ifdef DEBUG
#define LOG_DEBUG(format, ...)      LOG_Print(__FUNCTION__, LOG_LEVEL_INFO, format, ##__VA_ARGS__)
#endif

#define LOG_INFO(format, ...)       LOG_Print(__FUNCTION__, LOG_LEVEL_INFO, format, ##__VA_ARGS__)
#define LOG_WARNING(format, ...)    LOG_Print(__FUNCTION__, LOG_LEVEL_WARNING, format, ##__VA_ARGS__)
#define LOG_FATAL(format, ...)      LOG_Print(__FUNCTION__, LOG_LEVEL_FATAL, format, ##__VA_ARGS__)

#define ASSERT(format, ...) \
do {        \
            \
} while(1)

#endif