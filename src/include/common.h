/**************************************************************************//**
 * @file     common.h
 * @brief    通用定义
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _COMMON_H_
#define _COMMON_H_

#include "stdint.h"
#include "stddef.h"

/* 错误码 */
#define RET_OK      0
#define RET_ERROR   1
#define RET_BUSY    2
#define RET_TIMEOUT 3

#define ENABLE      1
#define DISABLE     0

#define TRUE        1
#define FALSE       0

#define MAX_CHAR_VALUE  0xFF
#define MAX_SHORT_VALUE 0xFFFF
#define MAX_INT_VALUE   0xFFFFFFFF

typedef unsigned int        uintptr_t;

#define __I     volatile const      /*!< Defines 'read only' permissions */
#define __O     volatile            /*!< Defines 'write only' permissions */
#define __IO    volatile            /*!< Defines 'read / write' permissions */

/* following defines should be used for structure members */
#define __IM    volatile const      /*! Defines 'read only' structure member permissions */
#define __OM    volatile            /*! Defines 'write only' structure member permissions */
#define __IOM   volatile            /*! Defines 'read / write' structure member permissions */

/*! bit定义 */
#define BIT(x)  (1u << x)

/*! 寄存器读取或设置 */
#define REG32_READ(addr)        (*(volatile uint32_t *)(uintptr_t)addr)
#define REG32_WRITE(addr, val)  (*(volatile uint32_t *)(uintptr_t)addr = (val))

/*! bit域操作，val=配置值，offset=偏移，width=位宽 */
#define BIT_MASK_GET(val, offset, width)

/*! 获取大值或者小值 */
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#define MIN(x,y) (((x) < (y)) ? (x) : (y))

/*! 得到一个field在结构体(struct)中的偏移量 */
#define OFFSET_OF(type, field) ((uint32_t)(&(((type *)0)->field)))

/*! 字节提取操作 */
#define LOBYTE(x)   ((uint8_t)((x) & 0xFF))                 /*! 取16位数的低8位 */
#define HIBYTE(x)   ((uint8_t)(((x) & 0xFF00) >> 8))        /*! 取16位数的高8位 */
#define LOWORD(x)   ((uint16_t)((x) & 0x0000FFFF))          /*! 取32位数的低16位 */
#define HIWORD(x)   ((uint16_t)(((x) & 0xFFFF0000) >> 16))  /*! 取32位数的高16位 */
#define DBYTE0(x)   LOBYTE(LOWORD(x))   /*! 取32位的第Byte0 */
#define DBYTE1(x)   HIBYTE(LOWORD(x))   /*! 取32位的第Byte1 */
#define DBYTE2(x)   LOBYTE(HIWORD(x))   /*! 取32位的第Byte2 */
#define DBYTE3(x)   HIBYTE(HIWORD(x))   /*! 取32位的第Byte3 */

/*! 拼接word和dword */
#define MAKEWORD(lo, hi)    ((uint16_t)((((uint32_t)(lo)) & 0xFF) | ((((uint32_t)(hi)) & 0xFF) << 8)))
#define MAKEDWORD(lo, hi)   ((uint16_t)((((uint32_t)(lo)) & 0xFFFF) | ((((uint32_t)(hi)) & 0xFFFF) << 16)))
#define MAKEDWORD_B4(b0, b1, b2, b3)    \
    (uint32_t)((((uint32_t)(b0)) & 0xFF) | ((((uint32_t)(b1)) & 0xFF) << 8) | \
               ((((uint32_t)(b2)) & 0xFF) << 16) | ((((uint32_t)(b3)) & 0xFF) << 24))

/*! 返回数组元素的个数 */
#define ARR_SIZE(a)  (sizeof((a)) / sizeof((a[0])))

#endif  // COMMON_H