/**************************************************************************//**
 * @file     sys_api.h
 * @brief    系统类接口
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2004, 2005 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _SYS_API_H_
#define _SYS_API_H_

#include "common.h"

typedef enum {
    SYS_RST_TYPE_PIN = 0,   /*!< NRST引脚复位 */
    SYS_RST_TYPE_POR,       /*!< 上电/掉电复位 */
    SYS_RST_TYPE_SOFT,      /*!< 软件复位 */
    SYS_RST_TYPE_IWDG,      /*!< 独立看门狗复位 */
    SYS_RST_TYPE_WWDG,      /*!< 窗口看门狗复位 */
    SYS_RST_TYPE_LPWR,      /*!< 低功耗复位 */
    SYS_RST_TYPE_BUTT
} EN_SYS_RST_TYPE;

uint32_t SYS_SysTickGet(void);
void SYS_DelayMs(uint32_t delay);
uint32_t SYS_ResetTypeGet(void);

#endif