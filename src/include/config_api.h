/**************************************************************************//**
 * @file     config_api.h
 * @brief    工程配置信息
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _CONFIG_H_
#define _CONFIG_H_

#define CFG_HSE_FREQ    ((uint32_t)8000000)    /*!< Value of the External oscillator in Hz */
#define CFG_LSE_FREQ    ((uint32_t)32768)      /*!< Value of the External oscillator in Hz*/

/*! Vector Table base offset field. This value must be a multiple of 0x200. */
#define VECT_TAB_OFFSET  0x00000000U        /* 当前系统的中断向量表偏移，默认是0 */

#endif