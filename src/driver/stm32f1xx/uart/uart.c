
/******************************************************************************
 * @file     uart.c
 * @brief    stm32f1xx uart 模块操作接口
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#include "common.h"
#include "hal_stm32f1xx_uart_api.h"

#include "uart.h"

// UART对应的寄存器地址
USART_TypeDef *g_pstUsartReg[USART_IDX_BUTT] = {USART1, USART2, USART3};

// UART全局变量结构体
ST_USART_INFO g_stUsartInfo[USART_IDX_BUTT];

/**
 * @brief  USART同步打印一个字符
 * @note   一般用于系统启动早期
 * @param  usart 要使用的串口
 * @param  pData 要输出的数据BUF
 * @param  Size  最大要输出的数据长度，或者遇到'\0'也会停止输出
 * @retval 无
 */
void USART_SyncTransmit(EN_USART_IDX usart, char *pData)
{
    uint32_t offset = 0;
    char transmitChar;
    USART_TypeDef *pUsart = g_pstUsartReg[usart];

    while (1) {
        transmitChar = pData[offset];
        if (transmitChar == '\0') {
            // 如果打印到结束符，直接退出
            break;
        }

        // 将一个字节的数据送到数据寄存器DR中，对DR的写操作会使得TXE位变成0
        pUsart->DR.DR = transmitChar;

        // 等待数据从数据寄存器搬移到移位寄存器，1=搬移完成
        while (pUsart->SR.TXE != 1);

        // 移动偏移，继续发送下一个字节
        offset++;
    }

    // TC位标识最后一个字节是否发送完成，1=发送完成
    while (pUsart->SR.TC != 1);

    return;
}

/**
 * @brief  USART初始化配置函数
 * @note   无
 * @param  pstUsartInit 初始化结构体
 * @retval 无
 */
uint32_t USART_Init(ST_USART_CTRL *pstUsartInit)
{
    return 0;
}