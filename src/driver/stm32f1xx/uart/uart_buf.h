/**************************************************************************//**
 * @file     uart_buf.h
 * @brief    stm32f1xx uart BUF 管理
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _UART_BUF_H_
#define _UART_BUF_H_

#include "common.h"

#define UART_TRANSMIT_BUF_MAX_LEN    512     // 串口发送BUF缓冲区的深度
#define UART_RECEIVE_BUF_MAX_LEN     128     // 串口接收BUF缓冲区的深度

typedef struct {
    uint32_t head;      // 当前待处理的第一个字符
    uint32_t tail;      // 永远指向第一个可以使用的空闲位
    char *pBuf;         // 保存BUF
    uint32_t bufLen;    // BUF长度
} ST_USART_BUF_CTRL;

void UartTransmitBufSave(char *pSaveBuf);
char UartTransmitBufLoad(void);

#endif