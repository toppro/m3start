/**************************************************************************//**
 * @file     rcc.c
 * @brief    stm32f1xx rcc 模块操作接口
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _RCC_H_
#define _RCC_H_

#define RCC_CLK_WAIT_TIMEOUT    0x2000000   // 等待时钟生效的最大超时时间，经验值

#endif