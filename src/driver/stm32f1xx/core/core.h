/**************************************************************************//**
 * @file     core.h
 * @brief    stm32f1xx内核操作接口
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _CORE_H_
#define _CORE_H_

/* Memory mapping of Core Hardware */

/* 
 * Private Peripheral Bus (address range 0xE0000000 to 0xE00FFFFF)
 * 
 * ***** 专用外设总线 （内部）   0xE0000000 ~ 0xE003FFFF 
 * ITM = Instrumentation Trace Macrocell    0xE0000000-0xE0000FFF profiling and performance monitor support
 * DWT = Data Watchpoint and Trace          0xE0001000-0xE0001FFF includes control for trace support
 * FPB = Flash Patch and Breakpoint         0xE0002000-0xE0002FFF optional block
 * SCS = System Control Space                                  0xE000E000 ~
    SCnSCB = System Control and ID Register not in the SCB      0xE000E000 ~
    NVIC = Nested Vectored Interrupt Controller                 0xE000E100 ~    内嵌向量中断控制器
 * SCB = System Control Block               0xE000ED00-0xE000ED8F SCB: generic control features
 * DCB = Debug Control Block                0xE000EDF0-0xE000EEFF debug control and configuration
 * 
 * ***** 专用外设总线 （外部）   0xE0040000 ~ 0xE00FFFFF 
 * TPIU = Trace Port Interface Unit         0xE0040000-0xE0040FFF optional trace and/or serial wire viewer support (see notes)
 * ETM = Embedded Trace Macrocell           0xE0041000-0xE0041FFF optional instruction trace capability
 * ARMv7-M ROM table                        0xE00FF000-0xE00FFFFF DAP accessible for auto-configuration
 */

// #define SCS_BASE       (0xE000E000UL)           /*!< System Control Space Base Address */
// #define ITM_BASE       (0xE0000000UL)           /*!< ITM Base Address */
// #define DWT_BASE       (0xE0001000UL)           /*!< DWT Base Address */
// #define TPI_BASE       (0xE0040000UL)           /*!< TPI Base Address */
// #define CoreDebug_BASE (0xE000EDF0UL)           /*!< Core Debug Base Address */
// #define SysTick_BASE   (SCS_BASE +  0x0010UL)   /*!< 0xE000_E010 SysTick Base Address */
// #define NVIC_BASE      (SCS_BASE +  0x0100UL)   /*!< 0xE000_E100 NVIC Base Address */
// #define SCB_BASE       (SCS_BASE +  0x0D00UL)   /*!< 0xE000_ED00 System Control Block Base Address */

// #define ITM        ((ITM_Type       *)ITM_BASE      ) /*!< 0xE000_0000 ITM configuration struct */
// #define DWT        ((DWT_Type       *)DWT_BASE      ) /*!< 0xE000_1000 DWT configuration struct */
// #define SCnSCB     ((SCnSCB_Type    *)SCS_BASE      ) /*!< 0xE000_E000 System control Register not in SCB */
// #define SysTick    ((SysTick_Type   *)SysTick_BASE  ) /*!< 0xE000_E010 SysTick configuration struct */
// #define NVIC       ((NVIC_Type      *)NVIC_BASE     ) /*!< 0xE000_E100 NVIC configuration struct */
// #define SCB        ((SCB_Type       *)SCB_BASE      ) /*!< 0xE000_ED00 SCB configuration struct */
// #define CoreDebug  ((CoreDebug_Type *)CoreDebug_BASE) /*!< 0xE000_EDF0 Core Debug configuration struct */
// #define TPI        ((TPI_Type       *)TPI_BASE      ) /*!< 0xE004_0000 TPI configuration struct */

#endif