/**************************************************************************//**
 * @file     hal_stm32f1xx_core_api.h
 * @brief    stm32f1xx 内核模块对外提供的公共接口
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2004, 2005 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _HAL_STM32F1XX_CORE_API_H_
#define _HAL_STM32F1XX_CORE_API_H_

/* 
 * M3的中断优先级分为pre-emption priority（抢占优先级/主优先级/组优先级）subpriority（子优先级/执行优先级）
 * 抢占优先级高的中断可以在低优先级中断执行过程中插入，而子优先级无法抢占，只是在同时发生时优先执行，
 * SCB->AIRCR寄存器中的bit[10:8] PRIGROUP域段用于设置中断优先级分组，比如 3的意思是从bit3开始分，意味着：
 * [7:4]给抢占优先级，[3:0]给子优先级，即抢占优先级等级有16个，子优先级等级也有16个。
 * STM32F1xx没有使用全部8bit，只用了高4bit表示中断优先级，所以有效配置范围是3~7
 * 除系统保留中断外，其它每个中断的具体优先级是通过配置 NVIC 的 IP 寄存器组来设置的，
 */
/*! CORTEX_Preemption_Priority_Group CORTEX Preemption Priority Group */
typedef enum {
    NVIC_PRIORITYGROUP_4 = 3,   /*!< 4 bits for pre-emption priority 0 bits for subpriority */
    NVIC_PRIORITYGROUP_3 = 4,   /*!< 3 bits for pre-emption priority 1 bits for subpriority */
    NVIC_PRIORITYGROUP_2 = 5,   /*!< 2 bits for pre-emption priority 2 bits for subpriority */
    NVIC_PRIORITYGROUP_1 = 6,   /*!< 1 bits for pre-emption priority 3 bits for subpriority */
    NVIC_PRIORITYGROUP_0 = 7    /*!< 0 bits for pre-emption priority 4 bits for subpriority */
} UN_CORE_NVIC_PRI_GP;

#endif