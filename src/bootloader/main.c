/**************************************************************************//**
 * @file     main.c
 * @brief    bootloader工程主体入口
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#include "common.h"
#include "config_api.h"
#include "sys_api.h"
#include "log_api.h"
#include "hal_stm32f1xx_gpio_api.h"
#include "hal_stm32f1xx_uart_api.h"

#include "sys_init.h"

void MainDelay(uint32_t delay)
{
    while(delay--);
    return;
}

/**
 * @brief  主体工程入口函数
 * @note   在跳转到此之前，需要先初始化好必要的环境
 * @param  无
 * @retval 无
 */
void Main(void)
{
    // 执行系统初始化
    SYS_Init();

    while (1) {
        LOG_INFO("STM32F103C8T6 is running.\r\n");
        SYS_DelayMs(1000);
    }

    return;
}