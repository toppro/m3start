/**************************************************************************//**
 * @file     sys_exception.h
 * @brief    系统异常和中断实现
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2004, 2005 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _SYS_EXCEPTION_H_
#define _SYS_EXCEPTION_H_

typedef void (*SYS_EXCEPTION_PF)(void);

#endif