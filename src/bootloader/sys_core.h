
/**************************************************************************//**
 * @file     sys_core.h
 * @brief    系统接口
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2004, 2005 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _SYS_CORE_H_
#define _SYS_CORE_H_

#include "common.h"

extern uint32_t g_SysTick;

uint32_t SYS_ResetTypeInit(void);

#endif