/**************************************************************************//**
 * @file     sys_init.h
 * @brief    系统初始化
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2004, 2005 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _SYS_INIT_H_
#define _SYS_INIT_H_

typedef uint32_t (*SYS_INIT_PF)(void);

void SYS_Init(void);

#endif