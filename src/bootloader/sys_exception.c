/**************************************************************************//**
 * @file     sys_exception.c
 * @brief    系统异常和中断实现
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2004, 2005 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#include "sys_core.h"
#include "sys_exception.h"

/******************************************************************************/
/*                    Cortex-M3 Processor 中断和异常处理函数                    */ 
/******************************************************************************/
// 不可屏蔽中断
void NMI_Handler(void)
{
    while(1);
}

// 所有被除能的 fault，都将“上访” (escalation)成硬 fault。只要FAULTMASK 没有置位，
// 硬 fault 服务例程就被强制执行。 Fault被除能的原因包括被禁用，或者被 PRIMASK/BASEPRI 被掩蔽。
// 若 FAULTMASK 也置位，则硬 fault 也被除能，此时彻底“关中”
void HardFault_Handler(void)
{
    while(1);
}

// 存储器管理 fault，MPU 访问违例以及访问非法位置均可引发。企图在“非执行区”取指也会引发此 fault
void MemManage_Handler(void)
{
    while(1);
}

// 从总线系统收到了错误响应，原因可以是预取流产（Abort）或数据流产，企图访问协处理器也会引发此 fault
void BusFault_Handler(void)
{
    while(1);
}

// 由于程序错误导致的异常。通常是使用了一条无效指令，或者是非法的状态转换，例如尝试切换到 ARM 状态
void UsageFault_Handler(void)
{
    while(1);
}

// 执行系统服务调用指令（SVC）引发的异常
void SVC_Handler(void)
{
    while(1);
}

// 调试监视器（断点，数据观察点，或者是外部调试请求）
void DebugMon_Handler(void)
{
    while(1);
}

// 为系统设备而设的“可悬挂请求”（pendable request）
void PendSV_Handler(void)
{
    while(1);
}

/******************************************************************************/
/*                         STM32F1XX 外部中断处理函数                           */ 
/******************************************************************************/
extern void SysTick_Handler(void);

/* 异常和中断向量表，用于重新定义中断向量表 */
SYS_EXCEPTION_PF g_apfSysExceptionTable[] = {
  NULL,                 // 默认的栈顶
//   NULL,                 // -15 优先级固定等于-3
//   NMI_Handler,          // -14 优先级固定等于-2
//   HardFault_Handler,    // -13 优先级固定等于-1
//   MemManage_Handler,    // -12
//   BusFault_Handler,     // -11
//   UsageFault_Handler,   // -10
//   NULL,
//   NULL,
//   NULL,
//   NULL,
//   SVC_Handler,          // -5 
//   DebugMon_Handler,     // -4 
//   NULL,
//   PendSV_Handler,       // -2 
//   SysTick_Handler,      // -1 
//   WWDG_IRQHandler       // 0 
//   PVD_IRQHandler
//   TAMPER_IRQHandler
//   RTC_IRQHandler
//   FLASH_IRQHandler
//   RCC_IRQHandler
//   EXTI0_IRQHandler
//   EXTI1_IRQHandler
//   EXTI2_IRQHandler
//   EXTI3_IRQHandler
//   EXTI4_IRQHandler      // 10
//   DMA1_Channel1_IRQHandler
//   DMA1_Channel2_IRQHandler
//   DMA1_Channel3_IRQHandler
//   DMA1_Channel4_IRQHandler
//   DMA1_Channel5_IRQHandler
//   DMA1_Channel6_IRQHandler
//   DMA1_Channel7_IRQHandler
//   ADC1_2_IRQHandler
//   USB_HP_CAN1_TX_IRQHandler
//   USB_LP_CAN1_RX0_IRQHandler  // 20
//   CAN1_RX1_IRQHandler
//   CAN1_SCE_IRQHandler
//   EXTI9_5_IRQHandler
//   TIM1_BRK_IRQHandler
//   TIM1_UP_IRQHandler
//   TIM1_TRG_COM_IRQHandler
//   TIM1_CC_IRQHandler
//   TIM2_IRQHandler
//   TIM3_IRQHandler
//   TIM4_IRQHandler     // 30
//   I2C1_EV_IRQHandler
//   I2C1_ER_IRQHandler
//   I2C2_EV_IRQHandler
//   I2C2_ER_IRQHandler
//   SPI1_IRQHandler
//   SPI2_IRQHandler
//   USART1_IRQHandler
//   USART2_IRQHandler
//   USART3_IRQHandler
//   EXTI15_10_IRQHandler  // 40
//   RTC_Alarm_IRQHandler
//   USBWakeUp_IRQHandler
//   0
//   0
//   0
//   0
//   0
//   0
//   0
//   BootRAM
};