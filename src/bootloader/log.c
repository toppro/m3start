/**************************************************************************//**
 * @file     log.c
 * @brief    log 模块
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "common.h"
#include "config_api.h"
#include "sys_api.h"
#include "log_api.h"
#include "hal_stm32f1xx_uart_api.h"

#include "log.h"

void LOG_Print(const char *pfuncName, EN_LOG_LEVEL level, const char *format, ...)
{
    uint32_t len = 0;
    char strFmt[LOG_LENGTH] = {0};
    va_list args;       //存放可变参数的数据结构

    len += snprintf(strFmt + len, LOG_LENGTH - len - 1, "[%lu]", SYS_SysTickGet());
    len += snprintf(strFmt + len, LOG_LENGTH - len - 1, "%s: ", pfuncName);

    //初始化可变参数,需要传一个va_list类型变量,和可变参数之前的参数,这里是str
    va_start(args, format);
    vsnprintf(strFmt + len, LOG_LENGTH -1, format, args);
    va_end(args);

    USART_SyncTransmit(USART_IDX1, strFmt);

    return;
}