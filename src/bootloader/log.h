/**************************************************************************//**
 * @file     log.h
 * @brief    bootloader工程主体入口
 * @version  V0.0.1
 * @date     2020-02-01
 ******************************************************************************/
/*
 * Copyright (c) 2004, 2005 by xiao xiang. All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software
 * is freely granted, provided that this notice is preserved.
 */
#ifndef _LOG_H_
#define _LOG_H_

#define LOG_LENGTH          64      // 单条日志最大支持输出的字符串长度，包括前缀

#endif