# 项目说明

## 项目简介

1. 此项目基于STM32F103C8T6芯片开发

2. 该项目包含了两个编译工程，bootloader和fw，其中bootloader用于引导fw固件，同时bootloader也支持IAP在线升级fw。

3. 项目采用arm-none-eabi-gcc 加makefle的方式编写

4. 项目大量参考了STM32CubeMX工程自动生成的代码，并复用了其相关代码和编译工程，也包括了CMSIS标准头文件，故相关标准定义、函数可以直接使用。

## 标准头文件定义

### core_cm3.h

* NVIC相关函数

``` C
NVIC_SetPriorityGrouping   // 设置中断优先级组
NVIC_GetPriorityGrouping   // 获取中断优先级组
NVIC_EnableIRQ             // 使能指定中断
NVIC_GetEnableIRQ          // 获取指定中断使能状态
NVIC_DisableIRQ            // 屏蔽指定中断
NVIC_GetPendingIRQ         // 获取指定中断是否处于pending状态
NVIC_SetPendingIRQ         // 手动设置指定中断进入pending状态
NVIC_ClearPendingIRQ       // 将指定中断从pengding状态中退出
NVIC_GetActive             // 判断指定中断是否处于激活状态
NVIC_SetPriority           // 设置指定中断的优先级，包括系统中断也可以通过该命令设置
NVIC_GetPriority           // 获取指定中断的优先级
NVIC_SystemReset           // 手动控制系统复位
NVIC_SetVector             // 如果中断向量表保存在SRAM，则可以通过该命令重新配置指定的中断向量函数
NVIC_GetVector             // 获取指定中断对应的中断向量函数
NVIC_EncodePriority        // 将参数组合成可以设置的中断优先级，传递给NVIC_SetPriority
NVIC_DecodePriority        // 将中断优先级解析成各个参数
```

* Systick相关函数

``` C
SysTick_Config              // 配置Systick值，并使能Systick，一般就启动的时候配置一次
```

* 标准内核寄存器定义

``` C
#define SCnSCB              ((SCnSCB_Type    *)     SCS_BASE      )   /*!< System control Register not in SCB */
#define SCB                 ((SCB_Type       *)     SCB_BASE      )   /*!< SCB configuration struct */
#define SysTick             ((SysTick_Type   *)     SysTick_BASE  )   /*!< SysTick configuration struct */
#define NVIC                ((NVIC_Type      *)     NVIC_BASE     )   /*!< NVIC configuration struct */
#define ITM                 ((ITM_Type       *)     ITM_BASE      )   /*!< ITM configuration struct */
#define DWT                 ((DWT_Type       *)     DWT_BASE      )   /*!< DWT configuration struct */
#define TPI                 ((TPI_Type       *)     TPI_BASE      )   /*!< TPI configuration struct */
#define CoreDebug           ((CoreDebug_Type *)     CoreDebug_BASE)   /*!< Core Debug configuration struct */
```

### cmsis_gcc.h

* 内核寄存器接口

``` C
__enable_irq        // 使能所有中断
__disable_irq       // 关闭所有中断
__get_CONTROL       // 读取control寄存器，control寄存器一般用于用户态和内核态切换
__set_CONTROL       // 配置control寄存器
__get_IPSR          // 读取IPSR中断号寄存器，用于获取当前中断号
__get_APSR          // 读取APSR寄存器，里面保存了上条指令结果，比如 N/Z/C/V/Q标识
__get_xPSR          // 一次性读取IPSR/APSR/EPSR（如果有）
__get_PSP           // 获取进程堆栈指针
__set_PSP           // 设置进程堆栈指针
__get_MSP           // 获取主堆栈指针
__set_MSP           // 设置主堆栈指针
__get_PRIMASK       // 获取PRIMASK比特位状态，0=没有关中断，1=屏蔽除NMI和HardFault以外的所有中断
__set_PRIMASK       // 配置PRIMASK位
```

* 内核指令

``` C
__NOP       // 空指令
__WFI       // Wait For Interrupt   CPU休眠指令
__WFE       // Wait For Event       和WFI类似，但可以被SEV指令唤醒
__SEV       // Send Event           一般用于多核唤醒
__ISB       // Instruction Synchronization Barrier 数据一致性同步，效果和代价都是ISB>DSB>DMB
__DSB       // Data Synchronization Barrier
__DMB       // Data Memory Barrier
__REV       // 32位Byte字节序转换，比如可以将 0x12345678 转换成 0x78563412.
__REV16     // 32位Word字节序转换，比如可以将 0x12345678 转换成 0x34127856.
__REVSH     // 16位字节序转换，比如可以将 0x1234 转换成 0x3412.
__ROR       // 向右循环移位操作
__BKPT      // 设置断点，用于调试
__RBIT      // DWORD比特位反转，比如0x0000_0005，反转后为0xA000_0000
__CLZ       // 计算前导位连续为0的数量，比如0x1FFF_FFFF，返回3
```

## 注意点

1. 由于默认采用的是小端模式，字节序按照小端定义，即定义的第一个bit是bit0。如果要在大端上用，比如重新定义。