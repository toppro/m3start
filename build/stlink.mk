# Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
#
# Permission to use, copy, modify, and distribute this software
# is freely granted, provided that this notice is preserved.

# 配置烧录工具
STLINKV2 = $(BUILD_DIR)/../tool/stlink/ST-LINK_CLI.exe
DOWNLOAD_TOOL = $(STLINKV2)
STLINK_SN = SN=56FF68064884494906410587 # ST-LINK对应的设备SN号
STLINK_ID = ID=0                        # ST-LINK对应的设备ID，0~9
DL_ADDR = 0x08000000	# 烧录地址

# 配置通用参数，用于指定 STLINK 烧录器
DL_COM = -c    # connect to device 该参数必须有
# DL_COM += $(STLINK_ID)    # 用SN还是ID都可以，如不指定默认是采用STLINK ID 0
DL_COM += SWD  # [SWD] or [JTAG] 默认采用JTAG
DL_COM += UR   # [UR]=Connect under reset [HOTPLUG]=Connect without halt or reset.
DL_COM += LPM  # Activate debug in low-power mode
# DL_COM += FREQ=4000 # 烧写频率，以KHz为单位，SWD默认为4MHz

# 烧录选项，用于将bin烧写到片子
DL_PROG =  RM=Srst	# [-Rst]=Reset the system [-HardRst]=Only for JTAG Pin15 valid
DL_PROG += -ME  # 烧录前擦除Flash全部空间
# DL_PROG += -Run # 执行代码，也可以指定地址执行，比如-run 0x08003000
DL_PROG += -V   # 执行校验，确保程序正确加载到了FLASH

# 复位选项，用于输出设备信息
DL_RST = -Rst

DL_COM := $(strip $(DL_COM)) # 删除多余的空格
DL_PROG := $(strip $(DL_PROG)) # 删除多余的空格