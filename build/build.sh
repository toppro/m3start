#!/bin/sh

BUILD_DIR=`pwd`
MAKE='make -j5'

clear

# 判断是否有传入入参
if [ $# != 2 ]; then
    echo "Error command format:"
    echo "./build.sh (bootloader | fw | all) (clean | debug | release)"
    echo "Try bootload debug"
    TARGET=bootloader
    ACTION=debug
else
    TARGET=$1
    ACTION=$2
fi


if [ ${ACTION} = 'debug' ];
then
    DEBUG=1
else
    DEBUG=0
fi

echo "Try to build ${BUILD_DIR}/${TARGET} ${ACTION}"

if [ ${ACTION} = 'clean' ];
then
    if [ ${TARGET} = 'all' ];
    then
        echo "make clean all target"
        ${MAKE} -f bootloader/bootloader.mk clean
        ${MAKE} -f fw/fw.mk clean
    else
        echo "make clean target ${TARGET}"
        ${MAKE} -f ${TARGET}/${TARGET}.mk clean
    fi
else
    if [ ${TARGET} = 'all' ];
    then
        echo "make build all target"
        ${MAKE} -f bootloader/bootloader.mk all DEBUG=${DEBUG}
        ${MAKE} -f fw/fw.mk all DEBUG=${DEBUG}
    else
        echo "make build target ${TARGET}"
        ${MAKE} -f ${TARGET}/${TARGET}.mk all DEBUG=${DEBUG}
    fi
fi
