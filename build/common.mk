# Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
#
# Permission to use, copy, modify, and distribute this software
# is freely granted, provided that this notice is preserved.

# 配置目录
BUILD_DIR = .

# 公共头文件目录
API_DIR = $(BUILD_DIR)/../src/include
DRIVER_API_DIR = $(BUILD_DIR)/../src/driver/stm32f1xx/include
DRIVER_CMSIS_ST_DIR = $(BUILD_DIR)/../src/driver/stm32f1xx/include/ST
DRIVER_CMSIS_ARM_DIR = $(BUILD_DIR)/../src/driver/stm32f1xx/include/CMSIS

# SRC目录
SRC_DIR = $(BUILD_DIR)/../src
SRC_DRIVER_DIR = $(SRC_DIR)/driver/stm32f1xx

# 配置编译工具命令
RM      = rm -f
MKDIR   = mkdir -p
PREFIX  = arm-none-eabi
CC      = $(PREFIX)-gcc
LD      = $(PREFIX)-ld
OBJCOPY = $(PREFIX)-objcopy
OBJDUMP = $(PREFIX)-objdump
OBJDUMP = $(PREFIX)-objdump
AR      = $(PREFIX)-ar
SIZE    = $(PREFIX)-size
STRIP	= $(PREFIX)-strip