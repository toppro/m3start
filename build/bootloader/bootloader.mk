# Copyright (c) 2020, 2020 by xiao xiang. All rights reserved.
# 
# Permission to use, copy, modify, and distribute this software
# is freely granted, provided that this notice is preserved.
include common.mk
include stlink.mk

# 由脚本传入，控制编译debug还是release版本，1：debug版本 0：release版本
DEBUG = 1

# 工程的名称及最后生成文件的名字
TARGET = bootloader

# 子工程代码目录
SRC_PRJ_DIR   = $(SRC_DIR)/$(TARGET)
BUILD_PRJ_DIR = $(BUILD_DIR)/$(TARGET)
OBJ_DIR       = $(BUILD_PRJ_DIR)/obj
BIN_DIR       = $(BUILD_PRJ_DIR)/bin

# C文件编译选项
ifeq ($(DEBUG),1)			# 如果DEBUG=1，则设置为Debug模式
  CFLAGS =  -O0
  CFLAGS += -g3
  CFLAGS += -DDebug
else
  CFLAGS =  -Os				# 尽量减少生成的固件体积，和O2大部分优化项一致，但针对体积进行优化
endif

CFLAGS += -mcpu=cortex-m3   # 目标为Cortex M3
CFLAGS += -mthumb           # 使用这个编译选项生成的目标文件是Thumb的，STM32采用的是该指令集
CFLAGS += -nostartfiles     # 链接的时候不使用标准系统的启动文件。
CFLAGS += -std=gnu11
CFLAGS += -c                # 只编译，不链接
CFLAGS += -ffunction-sections   # 使用此参数，则会使每个函数单独成为一段，比如函数func1()成为.text.func1段，配合LD的-Wl,--gc-sections参数使用，删除不使用的段
CFLAGS += -fdata-sections   # 使用此参数，则会使每个data单独成为一段，配合LD的-Wl,--gc-sections参数使用，删除不使用的段
CFLAGS += -Wall
CFLAGS += -MMD              # 生成文件关联的信息。包含目标文件所依赖的所有源代码，输出将导入到.d的文件里面(忽略标准库头文件)
CFLAGS += -MP               # 生成的依赖文件里面，依赖规则中的所有 .h 依赖项都会在该文件中生成一个伪目标，其不依赖任何其他依赖项。
                            # 该伪规则将避免删除了对应的头文件而没有更新 “Makefile” 去匹配新的依赖关系而导致 make 出错的情况出现。
CFLAGS += -mfloat-abi=soft  # STM32不带VFP浮点计算模块，所以需要采用软浮点
CFLAGS += -fstack-usage     # 生成栈的使用信息，保存在.su文件中
CFLAGS += --specs=nano.specs    # 采用精简库，而不是标准库，可以减DDEBUG少程序体积

CFLAGS += -DSTM32F103xB 	# STM32F103使用HAL库需要添加的宏 -DUSE_HAL_DRIVER -DSTM32F103xB

CFLAGS += -I$(API_DIR)		# 只包含公共头文件，其它目录原则上不允许包含
CFLAGS += -I$(DRIVER_API_DIR)
CFLAGS += -I$(DRIVER_CMSIS_ST_DIR)
CFLAGS += -I$(DRIVER_CMSIS_ARM_DIR)

CFLAGS := $(strip $(CFLAGS))	# 删除多余的空格

# LD链接选项
LDSCRIPT = "$(BUILD_PRJ_DIR)/$(TARGET).ld"	# 链接文件

LDFLAGS =  -mcpu=cortex-m3
LDFLAGS += --specs=nosys.specs
LDFLAGS += -Wl,-Map="$(BIN_DIR)/$(TARGET).map"		# 输出一个链接map文件
LDFLAGS += -Wl,--gc-sections	# 去除掉不用的section，配合CFLAGS里面的-ffunction-sections和-fdata-sections使用
LDFLAGS += -static				# 不连接动态共享库，比如.so文件
LDFLAGS += --specs=nano.specs
LDFLAGS += -mfloat-abi=soft
LDFLAGS += -mthumb
LDFLAGS += -Wl,--start-group -lc -lm -Wl,--end-group
LDFLAGS += -T$(LDSCRIPT)

LDFLAGS := $(strip $(LDFLAGS))	# 删除多余的空格

# 需要参与编译的.c文件，因为驱动很大，如果不需要的.c就不要编进来
# 也可以采用语句： SRCS = $(wildcard $(SRC_PRJ_DIR)/*.c)
SRCS =  $(shell find $(SRC_PRJ_DIR) -name '*.c') \
		$(SRC_DRIVER_DIR)/gpio/gpio.c \
		$(SRC_DRIVER_DIR)/core/core.c \
		$(SRC_DRIVER_DIR)/rcc/rcc.c	\
		$(SRC_DRIVER_DIR)/uart/uart.c \
		$(SRC_DRIVER_DIR)/uart/uart_buf.c
SRCS_ASM = $(shell find $(SRC_PRJ_DIR) -name '*.S')

# 将所有.c和.S文件替换成.o
OBJS =  $(SRCS:.c=.o)
OBJS_ASM = $(SRCS_ASM:.S=.o)

# 开始编译 ================================================================================
.PHONY: all clean download

OUTPUT_DIRS = $(OBJ_DIR) $(BIN_DIR)

# 默认需要生成的文件
ELF_FILE = $(BIN_DIR)/$(TARGET).elf
BIN_FILE = $(BIN_DIR)/$(TARGET).bin
HEX_FILE = $(BIN_DIR)/$(TARGET).hex
ASM_FILE = $(BIN_DIR)/$(TARGET).asm

all: $(OUTPUT_DIRS) $(ELF_FILE) $(BIN_FILE) $(HEX_FILE) $(ASM_FILE)
    # 输出各段详细信息，-t：各段及汇总信息  -x：十六进制显示
	$(SIZE) -t -x $(ELF_FILE)
	@echo "##### Build success. Start to download fw..."
#	$(DOWNLOAD_TOOL) $(DL_COM) $(DL_FILE) -P $(BIN_FILE) $(DL_ADDR)
#	$(DOWNLOAD_TOOL) $(DL_COM) $(DL_RST)
#	openocd.exe -f interface/cmsis-dap.cfg -f target/stm32f1x.cfg -c "program $(ELF_FILE) verify reset exit"
	openocd.exe -f interface/cmsis-dap.cfg -f target/stm32f1x.cfg -c "program $(ELF_FILE) reset exit"

# 检查默认要存在的目录，没有则创建
$(OUTPUT_DIRS):
	@echo "##### Start to make output DIRs"
	$(MKDIR) $@

# 不能省略前面的$(OBJS)，否则会采用默认规则，直接把.o生成在和.c相同的目录下
# 通过notdir和addprefix函数，将.o文件放到指定位置
$(OBJS):%o:%c
	@echo "##### Start to compile C files $(OBJS)"
	$(CC) $(CFLAGS) -o $(addprefix $(OBJ_DIR)/, $(notdir $@)) $<

$(OBJS_ASM):%o:%S
	@echo "##### Start to compile ASM files $(OBJS_ASM)"
	$(CC) $(CFLAGS) -o $(addprefix $(OBJ_DIR)/, $(notdir $@)) $<

# gcc编译出来的是ELF格式，MDK编译出来的是AXF格式，它们都是可执行的二进制文件
$(ELF_FILE):$(OBJS) $(OBJS_ASM)
	@echo "##### Start to make ELF file"
	$(CC) -o $@ $(wildcard $(OBJ_DIR)/*.o) $(LDFLAGS)
    ifeq ($(DEBUG),0)
	  $(STRIP) $@
    endif

# 生成BIN格式文件
$(BIN_FILE):$(ELF_FILE)
	@echo "##### Start to make BIN file"
	$(OBJCOPY) -O binary $< $@

# 生成HEX格式文件
$(HEX_FILE):$(ELF_FILE)
	@echo "##### Start to make HEX file"
	$(OBJCOPY) -O ihex $< $@

# 生成反汇编文件
$(ASM_FILE):$(ELF_FILE)
	@echo "##### Start to make ASM file"
	$(OBJDUMP) -a -h -S -t $< > $@

clean:
	@echo "clean all $(TARGET) project objs"
	$(RM) $(OBJ_DIR)/*
	$(RM) $(BIN_DIR)/*
    $(shell find ~/project/m3start/ -name *.[od] -o -name *.su | xargs rm -rf)
